import { fightersDetails, fighters } from './mockData';
import { IFighterDetails } from './IFighterDetails';

const API_URL = 'https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/';
const useMockAPI = true;

export const enum httpMethod {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
  PUT = 'PUT'
}

function callApi<T>(endpoint: string, method: httpMethod): Promise<T> {
  const url: string = API_URL + endpoint;
  const options = {
    method,
  };

  console.log('API CALL: ', url, method, options);

  return useMockAPI
    ? fakeCallApi<T>(endpoint)
    : fetch(url, options)
        .then((response) => (
          response.ok 
            ? response.json() as Promise<{ data: T }>
            : Promise.reject(Error('Failed to load'))))
        .then(result => result.data)
        .catch((error: Error) => {
          throw error;
        });
}

function fakeCallApi<T>(endpoint: string): Promise<T> {
  let response : T;

  try {
    if (endpoint === 'fighters.json') {
      response = fighters as unknown as T;
    } else {
      response = getFighterById(endpoint) as unknown as T;
    }
  } catch (error) {
    return Promise.reject(Error('Type in fakeApiCall is wrong'));
  }

  return new Promise((resolve, reject) => {
    setTimeout(() => (response ? resolve(response) : reject(Error('Failed to load'))), 500);
  });
}

function getFighterById(endpoint: string): IFighterDetails {
  const start = endpoint.lastIndexOf('/');
  const end = endpoint.lastIndexOf('.json');
  const id = endpoint.substring(start + 1, end);

  const fighterDetails = fightersDetails.find(it => it._id === id);
  if (fighterDetails) {
    return fighterDetails;
  } else {
    throw new Error(`Fighter with id: ${id} does not exist`);
  }
}

export { callApi };
