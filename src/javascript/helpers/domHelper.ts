export const enum HTMLTag {
  DIV = 'div',
  IMG = 'img',
  BUTTON = 'button',
  SPAN = 'span',
  H1 = 'h1'
}

interface IAttributes {
  id?: string, 
  src?: string
}

interface ICreateElementParams { 
  tagName: HTMLTag,
  className: string,
  attributes?: IAttributes,
  title? :string,
  alt?: string
}

// eslint-disable-next-line @typescript-eslint/ban-types
const getKeyValue = <U extends keyof T, T extends object>(key: U) => (obj: T) =>
  obj[key];

export function createElement(obj: ICreateElementParams): HTMLElement {
  const { tagName, className, attributes = {} } = obj;
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  console.log(attributes);
  Object.keys(attributes).forEach(key => 
    element.setAttribute(key, getKeyValue<keyof IAttributes, IAttributes>(key as keyof IAttributes)(attributes) as string)
  );

  return element;
}
