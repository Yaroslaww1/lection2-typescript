import { controls } from '../../constants/controls';
import { IFighterDetails } from '../helpers/IFighterDetails';

const enum PlayerNumber {
  FIRST = 0,
  SECOND = 1,
}

function compareArray<T>(_array1: T[], _array2: T[]) {
  const array1: T[] = _array1.slice().sort();
  const array2: T[] = _array2.slice().sort();
  return array1.length === array2.length && array1.every((value, index) => value === array2[index]);
}

class Player {
  public isBlocking = false;
  private fighter: IFighterDetails;
  private keysPressed: string[];
  private health: number;
  private lastCriticalTime: number;

  constructor(fighter: IFighterDetails) {
    this.fighter = fighter;
    this.keysPressed = [];
    this.health = fighter.health;
    this.lastCriticalTime = 0;
  }

  receiveDamage(damage: number): void {
    this.health -= damage;
  }

  isAlive(): boolean {
    return this.health > 0.0 ? true : false;
  }

  handleKeyPress(keyboardEvent: KeyboardEvent): void {
    this.keysPressed.push(keyboardEvent.code);
    console.log(this.keysPressed);
    if (this.keysPressed.length >= 4)
      this.keysPressed = this.keysPressed.slice(1, 5);
  }

  canDealCritical(time: number, criticalComboKeysCombination: string[]): boolean {
    console.log(this.keysPressed, criticalComboKeysCombination, compareArray(this.keysPressed, criticalComboKeysCombination))
    if (!compareArray<string>(this.keysPressed, criticalComboKeysCombination))
      return false;
    if (Math.abs(time - this.lastCriticalTime) >= 10 * 1000) {
      return true;
    } else {
      return false;
    }
  }

  doCritical(time: number): void {
    this.lastCriticalTime = time;
  }

  getHealthInPercent(): number {
    return Math.floor(this.health * 100 / this.fighter.health);
  }
}

export async function fight(firstFighter: IFighterDetails, secondFighter: IFighterDetails): Promise<IFighterDetails> {
  return new Promise((resolve) => {
    const player1: Player = new Player(firstFighter);
    const player2: Player = new Player(secondFighter);

    const updateIndicators = updaterFighterIndicator();

    document.addEventListener('keyup', function(event: KeyboardEvent) {
      if (event.code === controls.PlayerOneBlock) {
        player1.isBlocking = false;
      }
      if (event.code === controls.PlayerTwoBlock) {
        player2.isBlocking = false;
      }
    });

    document.addEventListener('keydown', function(event: KeyboardEvent) {
      let damage = 0;
      switch (decidePlayerByButton(event)) {
        case PlayerNumber.FIRST: {
          player1.handleKeyPress(event);

          if (event.code === controls.PlayerOneBlock) {
            player1.isBlocking = true;
            break;
          }

          if (event.code === controls.PlayerOneAttack && !player1.isBlocking && !player2.isBlocking) 
            damage = getDamage(firstFighter, secondFighter);

          const currentTime: number = Date.now();
          if (player1.canDealCritical(currentTime, controls.PlayerOneCriticalHitCombination)) {
            damage = getCriticalDamage(firstFighter);
            player1.doCritical(currentTime);
          }

          player2.receiveDamage(damage);
          updateIndicators(PlayerNumber.SECOND, player2.getHealthInPercent());
          break;
        }
        case PlayerNumber.SECOND: {
          player2.handleKeyPress(event);

          if (event.code === controls.PlayerTwoBlock) {
            player2.isBlocking = true;
            break;
          }

          if (event.code === controls.PlayerTwoAttack && !player2.isBlocking && !player1.isBlocking)
            damage = getDamage(secondFighter, firstFighter);

          const currentTime = Date.now();
          if (player2.canDealCritical(currentTime, controls.PlayerTwoCriticalHitCombination)) {
            damage = getCriticalDamage(firstFighter);
            player2.doCritical(currentTime);
          }

          player1.receiveDamage(damage);
          updateIndicators(PlayerNumber.FIRST, player1.getHealthInPercent());
          break;
        }
      }
    
      if (!player1.isAlive()) {
        resolve(secondFighter);
      }

      if (!player2.isAlive()) {
        resolve(firstFighter);
      }
    });
  });
}

function decidePlayerByButton(keyboardEvent: KeyboardEvent): PlayerNumber | null {
  const keyCode = keyboardEvent.code;
  if (keyCode === controls.PlayerOneAttack || 
      keyCode === controls.PlayerOneBlock || 
      controls.PlayerOneCriticalHitCombination.includes(keyCode))
    return PlayerNumber.FIRST;
  if (keyCode === controls.PlayerTwoAttack || 
      keyCode === controls.PlayerTwoBlock || 
      controls.PlayerTwoCriticalHitCombination.includes(keyCode))
    return PlayerNumber.SECOND;
  return null;
}

function updaterFighterIndicator() {
  const firstFighterIndicator: HTMLElement = document.getElementById("left-fighter-indicator") as HTMLElement;
  const secondFighterIndicator: HTMLElement = document.getElementById("right-fighter-indicator") as HTMLElement;
  console.log(firstFighterIndicator)

  return function updateIndicator(fighter: PlayerNumber, newHealth: number) {
    console.log(fighter, newHealth)
    switch (fighter) {
      case PlayerNumber.FIRST:
        firstFighterIndicator.style.width = `${newHealth}%`;
        break;
      case PlayerNumber.SECOND:
        secondFighterIndicator.style.width = `${newHealth}%`;
        break;
      default:
        console.error(`Wrong fighter: ${fighter}`);
        break;
    }
  }
}

// return damage
function getDamage(attacker: IFighterDetails, defender: IFighterDetails): number {
  return Math.max(0, getHitPower(attacker) - getBlockPower(defender));
}

function getCriticalDamage(fighter: IFighterDetails): number {
  const power: number = fighter.attack * 2;
  return power;
}

// return hit power
function getHitPower(fighter: IFighterDetails): number {
  const criticalHitChance: number = Math.random() + 1;
  const power: number = fighter.attack * criticalHitChance;
  return power;
}

// return block power
function getBlockPower(fighter: IFighterDetails): number {
  const dodgeChance: number = Math.random() + 1;
  const power: number = fighter.defense * dodgeChance;
  return power;
}