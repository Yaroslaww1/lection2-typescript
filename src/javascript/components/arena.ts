import { createElement, HTMLTag } from '../helpers/domHelper';
import { createFighterImage, position } from './fighterPreview';
import { fight } from './fight';
import { showWinnerModal } from './modal/winner';
import { IFighterDetails } from '../helpers/IFighterDetails';

export function renderArena(fighter1: IFighterDetails, fighter2: IFighterDetails): void {
  const root: HTMLElement = document.getElementById('root') as HTMLElement;
  const arena: HTMLElement = createArena(fighter1, fighter2);

  root.innerHTML = '';
  root.append(arena);

  fight(fighter1, fighter2)
  .then((onResolved) => {
    console.log("resolved: ", onResolved);
    showWinnerModal(onResolved);
    return;
  })
  .catch((error) => {
    throw error;
  })
}

function createArena(fighter1: IFighterDetails, fighter2: IFighterDetails): HTMLElement {
  const arena = createElement({ 
    tagName: HTMLTag.DIV,
    className: 'arena___root'
  });
  const healthIndicators = createHealthIndicators(fighter1, fighter2);
  const fighters = createFighters(fighter1, fighter2);
  
  arena.append(healthIndicators, fighters);
  return arena;
}

function createHealthIndicators(leftFighter: IFighterDetails, rightFighter: IFighterDetails): HTMLElement {
  const healthIndicators: HTMLElement = createElement({ 
    tagName: HTMLTag.DIV,
    className: 'arena___fight-status'
  });
  const versusSign: HTMLElement = createElement({
    tagName: HTMLTag.DIV,
    className: 'arena___versus-sign'
  });
  const leftFighterIndicator = createHealthIndicator(leftFighter, position.LEFT);
  const rightFighterIndicator = createHealthIndicator(rightFighter, position.RIGHT);

  healthIndicators.append(leftFighterIndicator, versusSign, rightFighterIndicator);
  return healthIndicators;
}

function createHealthIndicator(fighter: IFighterDetails, position: position): HTMLElement {
  const { name } = fighter;
  const container: HTMLElement = createElement({
    tagName: HTMLTag.DIV,
    className: 'arena___fighter-indicator'
  });
  const fighterName: HTMLElement = createElement({
    tagName: HTMLTag.SPAN,
    className: 'arena___fighter-name'
  });
  const indicator: HTMLElement = createElement({
    tagName: HTMLTag.DIV,
    className: 'arena___health-indicator'
  });
  const bar: HTMLElement = createElement({
    tagName: HTMLTag.DIV,
    className: 'arena___health-bar',
    attributes: { id: `${position}-fighter-indicator` }
  });

  fighterName.innerText = name;
  indicator.append(bar);
  container.append(fighterName, indicator);

  return container;
}

function createFighters(fighter1: IFighterDetails, fighter2: IFighterDetails): HTMLElement {
  const battleField: HTMLElement = createElement({
    tagName: HTMLTag.DIV,
    className: `arena___battlefield`
  });
  const firstFighterElement: HTMLElement = createFighter(fighter1, position.LEFT);
  const secondFighterElement: HTMLElement = createFighter(fighter2, position.RIGHT);

  battleField.append(firstFighterElement, secondFighterElement);
  return battleField;
}

function createFighter(fighter: IFighterDetails, position: position.LEFT | position.RIGHT): HTMLElement {
  const imgElement: HTMLElement = createFighterImage(fighter);
  const positionClassName = `arena___${position}-fighter`;
  const fighterElement: HTMLElement = createElement({
    tagName: HTMLTag.DIV,
    className: `arena___fighter ${positionClassName}`,
  });

  fighterElement.append(imgElement);
  return fighterElement;
}
