import { createElement, HTMLTag } from '../../helpers/domHelper';

interface IModalParams { 
  title: string,
  bodyElement: HTMLElement,
  onClose: () => void,
}

export function showModal(obj: IModalParams): void {
  const { title, bodyElement, onClose } = obj;
  const root = getModalContainer() as HTMLElement;
  const modal = createModal({ title, bodyElement, onClose }); 
  
  root.append(modal);
}

function getModalContainer(): HTMLElement {
  return document.getElementById('root') as HTMLElement;
}

function createModal(obj: IModalParams): HTMLElement {
  const { title, bodyElement, onClose } = obj;
  const layer = createElement({
    tagName: HTMLTag.DIV,
    className: 'modal-layer'
  });
  const modalContainer = createElement({
    tagName: HTMLTag.DIV,
    className: 'modal-root'
  });
  const header = createHeader({ title, onClose });

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
}

interface IHeaderParams { 
  title: string,
  onClose: () => void,
}

function createHeader(obj: IHeaderParams): HTMLElement {
  const { title, onClose } = obj;
  const headerElement = createElement({
    tagName: HTMLTag.DIV,
    className: 'modal-header'
  });
  const titleElement = createElement({
    tagName: HTMLTag.SPAN,
    className: ''
  });
  const closeButton = createElement({
    tagName: HTMLTag.DIV,
    className: 'close-btn'
  });
  
  titleElement.innerText = title;
  closeButton.innerText = '×';
  
  const close = () => {
    hideModal();
    onClose();
  }
  closeButton.addEventListener('click', close);
  headerElement.append(title, closeButton);
  
  return headerElement;
}

function hideModal() {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
}
