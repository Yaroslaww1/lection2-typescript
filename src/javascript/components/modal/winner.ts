import { showModal } from './modal';
import { createElement, HTMLTag } from '../../helpers/domHelper'
import { IFighterDetails } from '../../helpers/IFighterDetails';

export function showWinnerModal(fighter: IFighterDetails): void {
  const bodyElement: HTMLElement = createElement({
    tagName: HTMLTag.H1,
    className: ''
  })
  bodyElement.innerHTML = `<h1> ${fighter.name} win. Congrats!`
  const modalData = {
    title: `${fighter.name} win`,
    bodyElement,
    onClose: () => {location.reload();}
  }
  showModal(modalData);
}
