import { createElement, HTMLTag } from '../helpers/domHelper';
import { renderArena } from './arena';
import * as versusImg from '../../../resources/versus.png';
import { createFighterPreview, position } from './fighterPreview';
import { fighterService } from '../services/fightersService';
import { IFighterDetails } from '../helpers/IFighterDetails';

export function createFightersSelector(): (event: MouseEvent, fighterId: string) => Promise<void> {
  let selectedFighters: Array<IFighterDetails> = [];

  return async (event: MouseEvent, fighterId: string) => {
    const fighter = await getFighterInfo(fighterId);
    const [playerOne, playerTwo] = selectedFighters;
    const firstFighter = playerOne ?? fighter;
    const secondFighter = playerOne ? playerTwo ?? fighter : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    renderSelectedFighters(selectedFighters);
  };
}

const fighterDetailsMap: Record<string, IFighterDetails> = {};

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap
  if (!fighterDetailsMap[fighterId]) {
    const fighter = await fighterService.getFighterDetails(fighterId);
    fighterDetailsMap[fighterId] = fighter;
  }
  
  const fighterDetails: IFighterDetails = fighterDetailsMap[fighterId];
  return fighterDetails;
}


function renderSelectedFighters(selectedFighters: IFighterDetails[]): void {
  const fightersPreview = document.querySelector('.preview-container___root');
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, position.LEFT);
  const secondPreview = createFighterPreview(playerTwo, position.RIGHT);
  const versusBlock = createVersusBlock(selectedFighters);

  if (fightersPreview) {
    fightersPreview.innerHTML = '';
    fightersPreview.append(firstPreview, versusBlock, secondPreview);
  }
}

function createVersusBlock(selectedFighters: IFighterDetails[]): HTMLElement {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters[0], selectedFighters[1]);
  const container = createElement({
    tagName: HTMLTag.DIV,
    className: 'preview-container___versus-block'
  });
  const image = createElement({
    tagName: HTMLTag.IMG,
    className: 'preview-container___versus-img',
    attributes: { src: versusImg.default },
  });
  const disabledBtn = canStartFight ? '' : 'disabled';
  const fightBtn = createElement({
    tagName: HTMLTag.BUTTON,
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener('click', onClick, false);
  fightBtn.innerText = 'Fight';
  container.append(image, fightBtn);

  return container;
}

function startFight(fighter1: IFighterDetails, fighter2: IFighterDetails): void {
  renderArena(fighter1, fighter2);
}
