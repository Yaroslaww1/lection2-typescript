import { createElement, HTMLTag } from '../helpers/domHelper';
import { createFightersSelector } from './fighterSelector';
import { IFighter } from '../helpers/IFighter';
import { IFighterDetails } from '../helpers/IFighterDetails';

export function createFighters(fighters: IFighter[]): HTMLElement {
  const selectFighter = createFightersSelector();
  const container = createElement({ 
    tagName: HTMLTag.DIV,
    className: 'fighters___root'
  });
  const preview = createElement({
    tagName: HTMLTag.DIV,
    className: 'preview-container___root'
  });
  const fightersList = createElement({
    tagName: HTMLTag.DIV,
    className: 'fighters___list'
  });
  const fighterElements = fighters.map((fighter) => createFighter(fighter, selectFighter));

  fightersList.append(...fighterElements);
  container.append(preview, fightersList);

  return container;
}

function createFighter(
  fighter: IFighter | IFighterDetails,
  selectFighter: (event: MouseEvent, fighterId: string) => Promise<void>
): HTMLElement {
  const fighterElement = createElement({
    tagName: HTMLTag.DIV,
    className: 'fighters___fighter'
  });
  const imageElement = createImage(fighter);
  const onClick = (event: MouseEvent) => selectFighter(event, fighter._id);

  fighterElement.append(imageElement);
  fighterElement.addEventListener('click', onClick, false);

  return fighterElement;
}

function createImage(fighter: IFighter | IFighterDetails) {
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: HTMLTag.IMG,
    className: 'fighter___fighter-image',
    title: name,
    alt: name,
    attributes
  });

  return imgElement;
}