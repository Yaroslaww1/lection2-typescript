import { createElement, HTMLTag } from '../helpers/domHelper';
import { IFighterDetails } from '../helpers/IFighterDetails';

export const enum position {
  RIGHT = 'right',
  LEFT = 'left',
  TOP = 'top',
  BOTTOM = 'bottom'
}

export function createFighterPreview(fighter: IFighterDetails, position: position): HTMLElement {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: HTMLTag.DIV,
    className: `fighter-preview___root ${positionClassName}`,
  });

  try {
    const { attack,  defense, health, name } = fighter;
    fighterElement.append(createFighterImage(fighter));
    const fighterDescriptionElement: HTMLElement = createElement({
      tagName: HTMLTag.DIV,
      className: `fighter-preview___description-container ${positionClassName}`
    })
    fighterElement.append(fighterDescriptionElement);
    [
      {
        description: "attack",
        value: attack
      }, 
      {
        description: "defense",
        value: defense
      }, 
      {
        description: "health",
        value: health
      }, 
      {
        description: "name",
        value: name
      }
    ].forEach(element => {
      console.log(element);
      const descriptionElement = createElement({
        tagName: HTMLTag.DIV,
        className: `fighter-preview___description ${positionClassName}`,
      });
      descriptionElement.innerText = `${element.description} : ${element.value}`;
      fighterDescriptionElement.append(descriptionElement);
    });

    return fighterElement;
  } catch (error) {
    return createElement({
      tagName: HTMLTag.DIV,
      className: `${positionClassName}`
    });
  }
}

export function createFighterImage(fighter: IFighterDetails): HTMLElement {
  console.log("fighter image: ", fighter, fighter.source);
  const { source, name } = fighter;
  const attributes = { src: source };
  const imgElement = createElement({
    tagName: HTMLTag.IMG,
    className: 'fighter-preview___img',
    title: name,
    alt: name,
    attributes,
  });

  return imgElement;
}
