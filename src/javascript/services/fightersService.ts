import { callApi, httpMethod } from '../helpers/apiHelper';
import { IFighter } from '../helpers/IFighter';
import { IFighterDetails } from '../helpers/IFighterDetails';

class FighterService {
  async getFighters(): Promise<IFighter[]> {
    const endpoint = 'fighters.json';
    const apiResult: IFighter[] = await callApi<IFighter[]>(endpoint, httpMethod.GET);

    return apiResult;
  }

  async getFighterDetails(id: string): Promise<IFighterDetails> {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult: IFighterDetails = await callApi<IFighterDetails>(endpoint, httpMethod.GET);

    return apiResult;
  }
}

export const fighterService = new FighterService();
